const { defineConfig } = require("cypress");
const cucumber = require('cypress-cucumber-preprocessor').default;


module.exports = defineConfig({
  e2e: {
    
      specPattern: 'cypress/e2e/Form_Page/*.feature',
      specPattern:'cypress/e2e/API_Testing/*.js'
    },
  },
  module.exports = (on, config) => {
    on('file:preprocessor', cucumber())
  }
);
