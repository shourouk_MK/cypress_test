const FormElements = require("../FormPageElements")
export class FormPage{

   
nom(nom_arg){
    cy.get(FormElements.FormPageLocators.nom_text).type(nom_arg)
    return

}

prenom(prenom_arg){
    cy.get(FormElements.FormPageLocators.prenom_text).type(prenom_arg)
    return
}

ville(ville_arg){
    cy.get(FormElements.FormPageLocators.ville_text).select(ville_arg)
    return
}
email(email_arg){
    cy.get(FormElements.FormPageLocators.email_text).type(email_arg)
    return

}

telephone(telephone_arg){
    cy.get(FormElements.FormPageLocators.telephone_text).type(telephone_arg)
    return
}


bouttonEnvoyer(){
    cy.get(FormElements.FormPageLocators.envoyer_button).click()
    return
}


pop_up_message(){
    cy.get(FormElements.FormPageLocators.confirmation_message).should('be.visible')
}

    
}