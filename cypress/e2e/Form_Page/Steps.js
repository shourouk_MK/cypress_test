import { Given, When, Then } from "cypress-cucumber-preprocessor/steps"
import FormPage from "../pages/FormPageElmentsActions/FormPage";
import { FormPageActions } from "../pages/FormPageElmentsActions/FormPageActions";


const formPage = new FormPage;


Given('ouvrir la page formulaire', () => {
    cy.visit('file:///C:/Users/smaksoudi/Downloads/ceWeekEnd%20(1).html')

})

When('ajouter le nom', () => {
    formPage.nom('admin')
})
And('ajouter le prenom', () => {
    formPage.prenom('admin')
})
And('selectionner une ville', () => {
    formPage.ville('lille')
})
And('ajouter un email', () => {
    formPage.email('eamil@com')
})
And('ajouter un numero telephone', () => {
    formPage.telephone('+33123456789')
})
Then('cliquer sur envoyer', () => {
    formPage.bouttonEnvoyer()
})
